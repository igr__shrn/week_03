package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * Содержит первое значение
     * и указывает на следующий элемент и предыдущий.
     */
    private Node<T> head;

    /**
     * Содержит последнее значение
     * и указывает на следующий элемент и предыдущий.
     */
    private Node<T> tail;

    /**
     * Размер списка.
     */
    private int size;

    /**
     * @param <T>
     */
    private static class Node<T> {
        /**
         * Предыдущий элемент.
         */
        private Node prev;
        /**
         * Текущее значение.
         */
        private T value;
        /**
         * Следующий элемент.
         */
        private Node next;

        /**
         * @param val
         */
        Node(final T val) {
            this.value = val;
        }

    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {

        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
            tail = node;
        } else {
            node.prev = tail;
            tail.next = node;
            tail = node;
        }

        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        findNode(index).value = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        Node<T> node = findNode(index);
        return node.value;
    }

    /**
     * Поиск узла по индексу.
     *
     * @param index
     * @return boolean
     */
    private Node<T> findNode(final int index) {

        Node<T> node = null;
        checkIndex(index);

        if (index < size / 2) {
            node = head;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;
            for (int i = 0; i < (size - 1) - index; i++) {
                node = node.prev;
            }
        }

        return node;
    }

    /**
     * Проверка на присутствие индекса в списке.
     *
     * @param index
     */
    private void checkIndex(final int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Вне массива.");
        }
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override

    public void remove(final int index) {
        removeElement(index);
        size--;
    }

    /**
     * Удаление элемента по индексу.
     *
     * @param index
     */
    private void removeElement(final int index) {
        checkIndex(index);

        if (size == 1) {
            head = null;
            tail = null;
            return;
        }

        if (index == 0) {
            Node next = head.next;
            if (next != null) {
                next.prev = null;
            }
            head = next;
            return;
        }

        if (index == size - 1) {
            Node prev = tail.prev;
            prev.next = null;
            tail = prev;
        } else {
            //  [i-1]  <prv[i]next>  [i+1]

            //  [i-1]next> [i+1]

            //  [i-1] <prev[i+1]

            Node remove = findNode(index);
            remove.prev.next = remove.next;
            remove.next.prev = remove.prev;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final Object value) {
        Node<T> node = head;

        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                return i;
            }
            node = node.next;
        }

        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
