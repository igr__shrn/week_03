package ru.edu;

public class ArraySimpleQueue<T> implements SimpleQueue<T> {
    /**
     * Очередь.
     */
    private T[] arr;

    /**
     * Размер очереди.
     */
    private int size;

    /**
     * Указывает на элемент, который первый в очереди на выход.
     */
    private int head = 0;

    /**
     * Указывает на последний элемент в очереди.
     */
    private int tail = 0;

    /**
     * Количество элементов в очереди.
     */
    private int count = 0;

    /**
     * @param capacity
     */
    ArraySimpleQueue(final int capacity) {
        arr = (T[]) new Object[capacity];
        size = capacity;
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (isQueueFull()) {
            throw new ArrayIndexOutOfBoundsException("Очередь заполнена.");
        }

        count++;
        arr[tail] = value;
        tail = (tail + 1) % size;

        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (isQueueEmpty()) {
            throw new NullPointerException("Очередь пустая.");
        }

        T element = arr[head];

        arr[head] = null;
        head = (head + 1) % size;
        count--;

        return element;
    }

    /**
     * @return true, если заполнена очередь.
     */
    private boolean isQueueFull() {
        if (size == count) {
            return true;
        }
        return false;
    }

    /**
     * @return true, если нет в очереди элементов.
     */
    private boolean isQueueEmpty() {
        if (count == 0) {
            return true;
        }
        return false;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (isQueueEmpty()) {
            return null;
        }
        return arr[head];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return size - count;
    }
}
