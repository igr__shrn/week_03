package ru.edu;

public class LinkedSimpleStack<T> implements SimpleStack<T> {

    /**
     * Содержит первое значение
     * и указывает на следующий элемент и предыдущий.
     */
    private Node<T> head = null;

    /**
     * Содержит последнее значение
     * и указывает на следующий элемент и предыдущий.
     */
    private Node<T> tail;

    /**
     * Размер стэка.
     */
    private int size;

    /**
     * Количество элементов в стэке.
     */
    private int count;

    LinkedSimpleStack(final int capacity) {
        size = capacity;
    }

    /**
     * @param <T>
     */
    public static class Node<T> {

        /**
         * Текущее значение.
         */
        private T value;

        /**
         * Следующий элемент.
         */
        private Node next;

        /**
         * @param val
         */
        Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в стэк.
     *
     * @param value элемент
     * @return true если удалось поместить элемент, false если места уже нет
     */
    @Override
    public boolean push(final T value) {
        if (isStackFull()) {
            throw new ArrayIndexOutOfBoundsException("Стэк заполнен.");
        }
        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
        }
        node.next = head;
        head = node;

        count++;
        return true;
    }

    /**
     * Получение и удаление элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T pop() {
        if (empty()) {
            throw new NullPointerException("Стэк пуст.");
        }

        Node node = head;
        head = head.next;
        count--;

        return (T) node.value;
    }

    /**
     * Получение БЕЗ удаления элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T peek() {
        if (empty()) {
            throw new NullPointerException("Стэк пуст.");
        }
        return head.value;
    }

    /**
     * Проверка пустой ли стэк.
     *
     * @return true если пустой
     */
    @Override
    public boolean empty() {
        if (count == 0) {
            return true;
        }
        return false;
    }

    /**
     * @return true, если заполнен стэк.
     */
    private boolean isStackFull() {
        if (size == count) {
            return true;
        }
        return false;
    }

    /**
     * Количество элементов в стэке.
     *
     * @return количество
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Количество элементов которое может уместиться в стэке.
     *
     * @return емкость
     */
    @Override
    public int capacity() {
        return size - count;
    }
}
