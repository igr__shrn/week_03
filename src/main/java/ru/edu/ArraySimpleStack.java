package ru.edu;

public class ArraySimpleStack<T> implements SimpleStack<T> {

    /**
     * Размер стэка.
     */
    private int size;

    /**
     * Стэк.
     */
    private T[] arr;

    /**
     * Количество элементов в стэке.
     */
    private int count;


    /**
     * @param capacity
     */
    public ArraySimpleStack(final int capacity) {
        arr = (T[]) new Object[capacity];
        size = capacity;
    }

    /**
     * Добавление элемента в стэк.
     *
     * @param value элемент
     * @return true если удалось поместить элемент, false если места уже нет
     */
    @Override
    public boolean push(final T value) {
        if (isStackFull()) {
            throw new ArrayIndexOutOfBoundsException("Очередь заполнена.");
        }
        arr[count++] = value;
        return true;
    }

    /**
     * @return true, если заполнен стэк.
     */
    private boolean isStackFull() {
        if (size == count) {
            return true;
        }
        return false;
    }

    /**
     * Получение и удаление элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T pop() {
        if (empty()) {
            throw new NullPointerException("Очередь пустая.");
        }

        T element = arr[count - 1];
        arr[count - 1] = null;
        count--;

        return element;
    }

    /**
     * Получение БЕЗ удаления элемента из стэка.
     *
     * @return элемент или null
     */
    @Override
    public T peek() {
        if (empty()) {
            throw new NullPointerException("Очередь пустая.");
        }
        return arr[count - 1];
    }

    /**
     * Проверка пустой ли стэк.
     *
     * @return true если пустой
     */
    @Override
    public boolean empty() {
        if (count == 0) {
            return true;
        }
        return false;
    }

    /**
     * Количество элементов в стэке.
     *
     * @return количество
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Количество элементов которое может уместиться в стэке.
     *
     * @return емкость
     */
    @Override
    public int capacity() {
        return size - count;
    }
}
