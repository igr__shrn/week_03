package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Содержит первое значение
     * и хранит ссылки на следующие элементы.
     */
    private Node<T> head = null;

    /**
     * Содержит последнее значение.
     */
    private Node<T> tail = null;

    /**
     * Размер очереди.
     */
    private int size;

    /**
     * Количество элементов в очереди.
     */
    private int count;

    LinkedSimpleQueue(final int capacity) {
        size = capacity;
    }

    /**
     * @param <T>
     */
    static class Node<T> {
        /**
         * Предыдущий элемент.
         */
        private Node prev;
        /**
         * Текущее значение.
         */
        private T value;
        /**
         * Следующий элемент.
         */
        private Node next;

        Node(final T val) {
            value = val;
        }
    }


    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (isQueueFull()) {
            throw new ArrayIndexOutOfBoundsException("Очередь заполнена.");
        }

        Node<T> node = new Node<T>(value);

        if (count == 0) {
            head = node;
        } else {
            tail.next = node;
        }

        tail = node;
        count++;

        return true;
    }

    /**
     * @return true, если заполнена очередь.
     */
    private boolean isQueueFull() {
        if (size == count) {
            return true;
        }
        return false;
    }

    /**
     * @return true, если нет в очереди элементов.
     */
    private boolean isQueueEmpty() {
        if (count == 0) {
            return true;
        }
        return false;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (isQueueEmpty()) {
            throw new NullPointerException("Очередь пустая.");
        }

        T element = head.value;
        head = head.next;
        count--;
        if (isQueueEmpty()) {
            tail = null;
        }
        return element;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (isQueueEmpty()) {
            throw new NullPointerException("Очередь пустая.");
        }
        return head.value;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return size - count;
    }
}
