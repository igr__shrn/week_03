package ru.edu;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class LinkedSimpleQueueTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    public static final String FIFTH = "Fifth";

    private LinkedSimpleQueue<String> queue;

    @Before
    public void setUp() throws Exception {
        queue = new LinkedSimpleQueue<>(4);
        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
    }

    @Test
    public void testOffer() {
        queue.offer(FOURTH);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testOfferOutOfBounds() {
        queue.offer(FOURTH);
        queue.offer(FIFTH);
    }

    @Test
    public void testPoll() {
        assertEquals(FIRST, queue.poll());
    }
    @Test
    public void testPollInQueueCountOne() {
        queue = new LinkedSimpleQueue<>(2);
        queue.offer(FIRST);
        assertEquals(FIRST, queue.poll());
    }

    @Test(expected = NullPointerException.class)
    public void testPollEmptyQueue() {
        queue = new LinkedSimpleQueue<>(4);
        queue.poll();
    }

    @Test
    public void testPeek() {
        assertEquals(FIRST, queue.peek());
    }

    @Test(expected = NullPointerException.class)
    public void testPeekEmptyQueue() {
        queue = new LinkedSimpleQueue<>(4);
        queue.peek();
    }

    @Test
    public void testSize(){
        assertEquals(3, queue.size());
    }

    @Test
    public void testSizeEmpty(){
        queue = new LinkedSimpleQueue<>(4);
        assertEquals(0, queue.size());
    }

    @Test
    public void testCapacity(){
        assertEquals(1, queue.capacity());
    }
}