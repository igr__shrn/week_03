package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";

    private ArraySimpleQueue<String> queue;

    @Before
    public void setUp() {
        queue = new ArraySimpleQueue<>(4);
        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
    }

    @Test
    public void testOffer() {
        queue.offer(FOURTH);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testOfferOutOfBounds() {
        queue.offer(FOURTH);
        queue.offer(FOURTH);
        queue.offer(FOURTH);
    }

    @Test
    public void testPoll() {
        assertEquals(FIRST, queue.poll());
        assertEquals(SECOND, queue.poll());
        assertEquals(THIRD, queue.poll());
    }

    @Test
    public void testPollBig() {
        SimpleQueue queue = new ArraySimpleQueue<Integer>(5);
        int lastPoll = -1;
        for(int i = 0; i < 10; ++i){
            if(i > 4) {
                assertEquals(++lastPoll, queue.poll());
            }
            queue.offer(i);
        }
    }

    @Test(expected = NullPointerException.class)
    public void testPoolOutOfBounds() {
        queue = new ArraySimpleQueue<>(4);
        queue.poll();
    }

    @Test
    public void testPeek() {
        assertEquals(FIRST, queue.peek());
    }

    @Test
    public void testPeekNullValue() {
        queue = new ArraySimpleQueue<>(4);
        assertEquals(null, queue.peek());
    }

    @Test
    public void testSize() {
        assertEquals(3, queue.size());
    }

    @Test
    public void testCapacity() {
        assertEquals(1, queue.capacity());
        queue.offer(FOURTH);
        assertEquals(0, queue.capacity());
    }
}