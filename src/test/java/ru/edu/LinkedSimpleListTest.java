package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleListTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    public static final String FIFTH = "Fifth";
    public static final String SIXTH = "Sixth";

    private SimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
    }

    @Test
    public void testAdd() {
        list.add(FOURTH);
        assertEquals(FOURTH, list.get(3));
    }

    @Test
    public void testSet() {
        list.set(0, FOURTH);
        assertEquals(FOURTH, list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexOutBounds() {
        list.set(5, "Test");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexNegative() {
        list.set(-1, "Test");
    }

    @Test
    public void testGet() {
        assertEquals(FIRST, list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexOutBounds() {
        list.get(5);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexNegative() {
        list.get(-1);
    }

    @Test
    public void testRemove() {
        list.remove(2);
        assertEquals(FIRST, list.get(0));
        assertEquals(SECOND, list.get(1));

        list.remove(1);
        assertEquals(FIRST, list.get(0));

        list.remove(0);
        assertEquals(0, list.size());

        SimpleList<String> list = new LinkedSimpleList<>();
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        list.add(FOURTH);
        list.add(FIFTH);
        list.add(SIXTH);
        list.remove(4);
        assertEquals(SIXTH, list.get(4));
        list.remove(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexOutBounds() {
        list.remove(5);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexNegative() {
        list.remove(-1);
    }

    @Test
    public void testIndexOf() {
        assertEquals(0, list.indexOf(FIRST));
        assertEquals(1, list.indexOf(SECOND));
        assertEquals(-1, list.indexOf("Test"));
    }

    @Test
    public void testBig() {
        LinkedSimpleList list = new LinkedSimpleList<>();
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }

        for (int i = 0; i < 10; i++) {
            assertEquals("Check for " + i, String.valueOf(i), list.get(i));
        }
    }

}