package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleStackTest {
    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";
    public static final String FIFTH = "Fifth";

    private LinkedSimpleStack<String> stack;

    @Before
    public void setUp()  {
        stack = new LinkedSimpleStack<>(4);
        stack.push(FIRST);
        stack.push(SECOND);
        stack.push(THIRD);
    }

    @Test
    public void testPush(){
        stack.push(FOURTH);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testPushOutOfBounds(){
        stack.push(FOURTH);
        stack.push(FIFTH);
    }

    @Test
    public void testPop(){
        assertEquals(THIRD, stack.pop());
        assertEquals(SECOND, stack.pop());
    }

    @Test(expected = NullPointerException.class)
    public void testPopEmptyStack(){
        stack = new LinkedSimpleStack<>(4);
        stack.pop();
    }

    @Test
    public void testPeek(){
        assertEquals(THIRD, stack.peek());
    }

    @Test(expected = NullPointerException.class)
    public void testPeekEmptyStack(){
        stack = new LinkedSimpleStack<>(4);
        stack.peek();
    }

    @Test
    public void testEmptyStack(){
        stack = new LinkedSimpleStack<>(4);
        assertTrue(stack.empty());
    }

    @Test
    public void testNotEmptyStack(){
        assertFalse(stack.empty());
    }

    @Test
    public void testSize(){
        assertEquals(3, stack.size());
    }

    @Test
    public void testCapacity(){
        assertEquals(1, stack.capacity());
    }
}