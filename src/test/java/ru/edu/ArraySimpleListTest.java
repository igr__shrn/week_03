package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleListTest {

    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String THIRD = "Third";
    public static final String FOURTH = "Fourth";

    private ArraySimpleList<String> list;

    @Before
    public void setUp() {
        list = new ArraySimpleList<>(3);
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
    }

    @Test
    public void testAdd() {
        list.add(FOURTH);
        assertEquals(FOURTH, list.get(3));
    }

    @Test
    public void testSet() {
        list.set(0, "IndexZero");
        assertEquals("IndexZero", list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexOutBounds() {
        assertEquals(3, list.size());
        list.set(3, "Test");
        list.set(5, "Test");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetIndexNegative() {
        list.set(-3, "Test");
    }

    @Test
    public void testGet() {
        assertEquals(FIRST, list.get(0));
        assertEquals(SECOND, list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexOutBounds() {
        assertEquals(3, list.size());
        list.get(3);
        list.get(5);
        list.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetIndexNegative() {
        list.get(-1);
    }

    @Test
    public void testIndexOf() {
        assertEquals(0, list.indexOf(FIRST));
        assertEquals(1, list.indexOf(SECOND));
        assertEquals(-1, list.indexOf("TEST"));
    }

    @Test
    public void testRemove() {
        list.remove(0);
        assertEquals(SECOND, list.get(0));
        assertEquals(0, list.indexOf(SECOND));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexOutBounds() {
        list.remove(4);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveIndexNegative() {
        list.remove(-4);
    }

    @Test
    public void testSize() {
        assertEquals(3, list.size());

        list.add(FOURTH);
        assertEquals(4, list.size());

        list.remove(3);
        assertEquals(3, list.size());
    }


}